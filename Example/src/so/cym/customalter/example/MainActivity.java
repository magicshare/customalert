package so.cym.customalter.example;


import so.cym.customalter.AlertWindow;
import so.cym.customalter.UIUtils;
import so.cym.customalter.UIUtils.TimePickerListener;
import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;
/**
 * 
 * @author hzcaoyanming
 *
 */
public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void buttonWithOK(View v){  
		UIUtils.showModalAlertWindowWithOK(MainActivity.this,
				"带有ok按钮的弹窗");	
	}  

	public void buttonWithTitle(View v){
		UIUtils.showAlertWindow(MainActivity.this,"你好","我是一个有title的弹框","确定",new AlertWindow.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				UIUtils.dismissAlertWindow();
				Toast.makeText(MainActivity.this, "点击确定", 1000).show();
			}
		},"取消",new AlertWindow.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				UIUtils.dismissAlertWindow();
			}
		});
	}

	public void buttonWithList(View v){
		final String [] gourd = new String[] {"大娃力壮术","二娃千里眼","三娃刀枪不入","四娃火攻","五娃洪击","六娃隐形","七娃神葫芦"};
		UIUtils.showAlertWindowWithList(MainActivity.this, "葫芦娃七兄弟", new ArrayAdapter<String>(MainActivity.this,  android.R.layout.simple_list_item_1, gourd),new  AlertWindow.OnListItemClickListener(){
			@Override
			public void onItemClick(View view, int which) {
				UIUtils.dismissAlertWindow();
				Toast.makeText(MainActivity.this,gourd[which]+"", 1000).show();
			}
		});
	}
	public void buttonDatePick(View v){  
		UIUtils.showTimePicker(MainActivity.this,new TimePickerListener() {
			@Override
			public void timeChecked(int mHour, int mMin) {
				Toast.makeText(MainActivity.this, "所选时间为："+mHour+"   :  "+mMin, 1000).show();
			}
		});
	}  
	public void buttonCustomView(View v){
		ImageView imageView = new ImageView(MainActivity.this);
		imageView.setImageResource(R.drawable.ic_launcher);
		UIUtils.showAlertWindowWithCustomView(MainActivity.this,imageView,true,true);
	}
}
